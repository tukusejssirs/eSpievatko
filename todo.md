# eSpievatko TODO

*alebo co vsetko je v plane*

## vsetko (noty, akordy, texty)


## notove zapisy

- [ ] ukladanie v .mscore (‘zdrojak’ ulozeny na webe je v .mscore)
- [ ] export do .html, .png, .svg

## akordy

- [ ] ukladanie v .cho (‘zdrojak’ ulozeny na webe je v .cho)
- [ ] export do .html, .pdf

## texty

- [ ] ukladanie v .txt/.cho (‘zdrojak’ ulozeny na webe je v .txt/.cho)
- [ ] export do .html, .pdf, .odt, ?.doc
