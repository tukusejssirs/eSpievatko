É Bože si svätý

d(A) _ gjd) D _ A _ A D
|:Bože si sväty:|(4.r) |:Ty si ten, čo tróm na chválach Izraela:|
d c d C
V bázni tvojej smiem stáť a pevne dôverovať,

C B A

d
uctievať a vyznávať, že ty si ten Boh, najsvätejší.

Budem spievať,

  

text: M. Tóth

  

pretože som spasený

 

d B _ c d B c d
1. |:Budem spievať, pretože som spasený. Budem spievať Ježiš je môj Pán.:|
d F d B F C

R: |:Onje nádejou v čase súženía, viem, môj Pán zachráni ma.:|

2. |:Budein spievať, pretože som slobodný. Budem spievať, Ježiš zvíťazil.:|
3. |:Budein spievať, pretože som radostný. Budem spievať, Ježiš ľúbi ma.:|

Na nebesiach aj na zemi znie hlas text: M. Tóth

D _ G _ o _ _ G _ e' A
Na nebesiach aj na zemi znie hlas, zástup anjelov chváli Baránka.
G D A G

Vôkol trónuje Božej slávyjas, tak pridaj sa k tísícom anjelov.

G o o A o G o u D G a

Pridaj sa k starcom, čo chvália ho. Pozdvihni hlas! Aké krásne je,

e ň* G D
keď Boží národ chváli svojho Kráľa. Buďme v chvále zjednotení,
D A e A e
v bázni prebývajme pred Pánom, jeho láska tá nás mení, v dnešný deň
_ A _ o G o _ A o

prmesme mu obeť chvál pre jeho trón. Len ty sám si hoden chvály,

G D A G_ D_ _ A H e A o _
Vládca zvrchovaný, nikto nie je tebe podobný zo srdca chválu vzdám ti.

372_ text: JKS 39

C d G C d G C G d G C G C G
Búvaj, Dieťa Krásne, uložené v jasle. Búvaj, búvaj, Pachoľa, milostivé Jezuľa!
C C' F C G d C

Budeme ťa kolísať, abys mohol dobre spať.

C d G C d G C G C F G C

Ježišku náš milý, aby sa ti snili veľmi krásne sny, veľmi krásne sny.

 

2. Drozdy a hrdličky, chystajte pesničky, nech sa Dieťa poteší, na tom našom salaši.

Spev škovránka, slávika, k tomu pekná muzika.
My budeme s vami spievať za jasľami Synu milému, Synu milému,
3. Hory ticho buďte, Dieťa nezobuďte, nech si ono podrieme, na slame a na sene.
Aj vy, milé fialky, zaváňajte do diaľky.
Zavejte mu vône, Pánovi na tróne, ticho, sladučko, ticho, sladučko,
Sám k nám dnes prichádzaš, sám dotýkaš sa sŕdc textsA. „emma, 1997

   

C d F G
Sám k nám dnes prichádzaš, sám dotýkaš sa sŕdc, sám tíšiš bolesť žiaľ, sám láskavý Kráľ.

