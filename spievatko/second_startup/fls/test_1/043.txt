C G a C F C
R: Môj Pán Ježiš ma miloval, až na smrť šiel, svoj život dal.
G a F C G' C GC
Môj Pán Ježiš dnes Teba zve, poď domov, vráť sa späť.

2. Jeho láska stále rany hojí, a ku spáse nám náruč otvára,
dnes ako predtým búrku upokoj í, môj Pán.
Jeho milosť stále ešte trvá, chce zmieriť ľudí s Otcom nebeským
a krvou splatiť dlh za vykúpených, môj Pán. R:

m Deň, čo deň túžim ísť za Tebou

D A h Ft* G A

I. Deň čo deň túžim ísť za Tebou, byť Tvojou(Tvojítn), Pane môj.
D A h F8 G A D
Deň čo deň vidím zas biedu len, buď so mnou, Pane môj.
h G A D h G A

R: Láska Ty večná s nami zostávaj a daj pokoj svoj všetkým nám.
h G A D h G A D

Pán, žehnaj nás, o to prosíme a skloň sa milosti-vo k nám.

2. Deň čo deň liečiš ma z mojich rán, si so mnou, Pane môj.
Deň čo deň vidíš zas biedu len, buď so mnou, Pane môj. R:

Je to krv čistá, svätá, krv Tvoja
c C_"'°" a' F _ d_ G' G
1. Je to krv čistá, svätá, krv Tvoja, čo život nám dá.

 

F"'°" _ G c e a _ d' f
Je to kw, čo očtstí, ako sneh belších nás učmí, posvätí.
i C C"“'" a' d' G' G C
Je to Tvoja kw čistá, predivná, môj Ježiš, si seba za nás dal.
2. Je to krv mocná, svätá,kw Tvoja, čo víťazstvo dá.
Je to krv, krv Kristova, v ktorej moc nad zlým sa skrýva, víťazi.
Je to Tvoja krv mocná, predivná, môj Ježiš, si seba za nás dal.

 

Ked' ťa Kristus volá, pod' a nečakaj hudba: Ondrej Lazar Tkáč, text; Peter Žaloudek

c F c _ F c _

1. Keď Ťa Kristus volá, poď a nečakaj, späť k tomu, čo bolo, sa nevracaj.
C e a F G C C'

F G
|:Kráčaj ďalej cestou, na ktorú svieti Pán, žeby si z nej nezišiel v ústrety hlbinám.:|
2. Konaj vždy tak, aby Kristus nemal žiaľ, že mu diabol znovu ovečku vzal.
|:Ovečka si po hlase svojho Pána zná, veď aj Ty to cítiš, keď Ta zavolá.:|

3. Ak však chceš íst' za ním, buď mu oddaný, má vždy z toho radosť, keď si pokorný.
|:Nemysli už na nič, len Jemu srdce daj, láskou nekonečnou odmení Ta Tvoj Pán.:|
Album: Kapucíni: To je život; album: Kapllcĺłlĺľ Prosím, nepýtaj sa ma

 

Nech ten chrám zaplaví svetlo sviec text; Marcel Šiškovič

A E
Nech ten chrám zaplaví svetlo sviec jagavé, nech nám z úst znejú zvolania jasavé.

Nech ten chrám naplní zástup sŕdc veselých, mužov, žien, detí, nech jasá svet celý.

Am: _ _ __ D(E) __ AçDFA
R: |:Lebo Pánje ž1vý:| |:ž1je v nás:| (2x) žije v nas.

