 

Hádanky (Raz, dva, hádaj kto ľúbi ma) text: Ondrej Studenec
C  n C a C7. o
1. Raz, dva, raz a dva, hádaj kto ľúbi ma, raz, dva, tri, štyri, kto pridá m1 sily,
F C'a C C'a C G7 C

884.

885.

886.

päť, šesť, sedem, osem, deväť, Ježiš je odpoveď, aleluja, rád Pána mám.

. A, B, A a B, kto prebýva vo mne, A, B, C, D, E, chcel by aj ku Tebe,
F, G, H, l, J, to predsa Ježišje, aleluja, rád Pána mám.

. K, L, L, M, N, kto stvoril tento deň, O, P, P, R, S, kto zvieratká a les,

T, U, U, V, Z, Boh stvoril celý svet, aleluja, rád Pána mám.

Album: Céčko: Nebojínze sa
hudba a text: Ondrej Studenec
C F

 

Hej, hej, nádej (Poďte, pristúpte sem ľudia)

d1 as gv gs C

"\.

CF_C_F_CdBC _FBFCdBC F
.° |:Hej, hej, hej, nádej a túžbu mám, že odpusteme zvíťazí, Pane, pomôž nám, zbav nás reťazí.:|
. Keby krídla narástli mi ihneď tam poletím, do kráľovstva dobra, ponad rieky, moria.
. Po takom kráľovstve, viem, túžim nielenja, tak spravme si ho doma, vhodnáje doba. R:

. Tolerancia je vzácna, chýba nám ako soľ, táto modlitba pomôže, verím, Pane môj. R:
Album: Céčko: Nebq/'ínze sa

\mmm >a

Hľadači seba (Smerom k neznámu)

 

a _ G a G aGa  AD
. Smerom k neznámu stojí každý nový deň kde radosť končí, tam padá náš tieň.
a'D G a

*Nu

h a
Hľadáme pre seba pevný krok a chlieb
Prečo hľadači svojho šťastia prehrajú a vyznávač krásy nám pokazí deň
a ten, čo chcel lásku, sa ľutuje sám. Stovky hľadačov seba sa míňajú v tme.

. Chyby druhých aj naše vlastné pozná Pán a starosti o chlieb sám Otec pozná.
Tak najprv hľadajte kráľovstvo Božie všetko ostatné vám potom Otec pridá.

 

Hľadanie rodiča (Prázdno je, Bože, chýbaš námjtba: Maroš Kachút, text: Milan Rúfus
D' _D=/c D' _a_ F _G

Prázdno je, Bože, chýbaš nám, prázdny je priestor tu 1 tam,

p' a _ G F _ B a D'

jeho hrôza 1 veleba, prázdno je, Bože, bez teba.

Prázdno je slze vo víčku, prázdno je uhľom v ohníčku,
prázdno je deťom v starobe, prázdno je tráve na hrobe.
D' D' Dz/E Dz/F Dz/E D*
kríž.
. Pomysli, Bože, pomysli, to nie ty - my sme odišli,
ale my predsa prídeme zo zeme k tebe, do zeme.
Veď ten, kto nežil na zemi, nevie, kde more pramení,
nemôže ani do neba, prázdno je, Bože, bez teba.
A preto maj nás trochu rád, keby si nebol pod i nad,
vo vnútri veci, okolo, všetko, čo je, by nebolo.
Album: Atlanta: Dlhý príbeh

 

G B
Chýba do nej tvoj tichý

a?
. Poďte, pristúpte sem ľudia, čas rýchlo uteká, svet je tak nádherný, nebuďme k sebe zlí.

G
cestou k ďalšiemu cieľu a k ďalšiemu dňu.

