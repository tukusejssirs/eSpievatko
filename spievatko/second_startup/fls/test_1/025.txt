2. Pán je tvoj strážca, ako tôňa ťa prikrýva. Za dňa ťa slnko nezraní, ani mesiac za noci.

3. Pán ťa chráni pred každým zlom, On ti chráni život.
Pán bedlí nad tebou, keď vchádzaš a keď vychádzaš.

F G C a G F C F G C a G F C
Pán bedlí nad tebou odteraz až naveky. Pomoc mi príde od Pána, ktorý stvoril nebo i zem.

m Stály, verný si jedine Ty
C F C d C a G
1 . Stály, verný si jedine Ty, sám si, môj Boh a Pán.
F _ Ý e ,ř F G c c?
R: Ja chválim a, stále chválim a, Ty si stály, verný Boh sám.

2. Ó, Pane, ja prázdne mám ruky, chcem Ťa stále viac poznávať,
Pritiahni ma, Pane, chcem kráčať stále s tebou
( d G) c

a -) ( ( )
1. |:Pritiahni ma, Pane,:| |:chcem kráčať stále s 'ląebou,:|
d(E) a

|:pritiahni ma, Pane, sám,:| |:drž ma pevne.:|
2. |:Poď a ved' ma, Pane,:| |:zo svojho kráľovstva,:|

|:do svojho tajomstva,:| |:kde objíma ma tvoja láska.:|

a(G',a) d(C,d) E a
R: Chcem v tebe radosť mať (3x) a šťastný byť.
a(G.a)

d(C,d) E a
Chcem Ťa vždy milovať (3x), Ty si môj Boh a môj Pán.

Radosť, čo príde, je Svätý Duch
C F C FGCaFCGC
1. Radosť, čo príde, je Svätý Duch. (4x)
2. Pokoj... 3. Láska... 4. Nádej...

m Zídení ako rodina sme spolu pred Tebou
D e h G D e h G A

e A e
Zídení ako rodina sme spolu pred Tebou. Jednotní spievame zo srdca Kráľovi kráľov.
h fl* h i* G e

G e h f*
Nech znie: Abba Otče, sväté meno máš. Nech znie: Abba Otče, vzácny si pre nás.

Stvor vo mne srdce čisté
D A G D D A G D

Stvor vo mne srdce čisté, ó,Bože môj, a priameho ducha obnov mi. (2x)

G A o p _ o _ A _ o _ o'
Neodháňaj ma preč od tváre Tvojej a Ducha m1 svojho neodnímaj.
A D F3* h e D

G A
A radosť mi vráť zo záchrany mojej a priameho ducha obnov mi.

Nie, Pane, nám, lež svojmu menu daj

G(A) E _
|:Nie, Pane, nám (2x) lež svojmu menu daj česť a slávu.:|

