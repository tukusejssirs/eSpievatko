Ó, Matka Pána, Matka rajských krás, Ty vieš hudbaa text: Jozef Ju/inek
G _ G C G D_7
l. O, Matka Pána, Matka rajských krás, Ty vieš, čo srdce páli.
Vlej radosti do duše zas, keď umára sa V žiali.
G

 

7

C a D' G
Do svojích rúk ber srdce blúdiace, bud' milostivá duši prosiacej.
c _G a _D DCG _D'G
R: Mária, Mária, Má-ri-a.
2. Pod' so mnou krajom môjho domova, do hájov vonných kvetov,
kde žehnala nás ruka otcova, do šírich diaľnych svetov.
Nech Tvoje svetlo všade vodí ma, bud' v žití pomoc moja jediná. R:

3. Pod' našou vlasťou, Matka najsladšia, rod verných Tvojích prosí,
ked” stáročia mu je už najdrahšia viera, čo v srdci nosí.
Ten národ Tvoj sa k Tebe obracia, on nádej svoju v Teba nestráca. R:

4. Poď k zomrelým, čo trpia muky zlé, vypočuj plač ich stály,
ved' Tvoja láska zľutovať sa vie nad dušou, čo Ťa chváli.
Ty bolesť moju, prosby moje znáš, Ty vypočuješ Zdravas, Otčenáš. R:

5. Poď k nemocným, čo trpia veľký žiaľ a smútok sním im z tvárí.
Vráť svetlo lásky, kde ho život vzal, daj požehnania dary.
Daj zdravia tam, kde duša odchádza, nech milosť Tvoja vždy nás sprevádza. R:

6. Žiar kvetov bielych vencov nádhery Tvoj stánok krásne zdobí
a verných sŕdc tie nežné údery sú vďakou našej doby,
celého sveta láska Mária, spieva Ti vrúcne, slávne Glória. R:

Ó, Pane, ja musím ísť za Tebou m
, d' _ g' d' B' _ a' _ d' a
R: O, Pane, ja musím ísť za Tebou, bez Teba život prázdny ja mám.
p, d1 . 1 d7 ' Bor . a7 . d7
O, Pane, ja musím ísť za Tebou, svoj život, svoje túžby Ti dám.
97 C7 F d7
1. Neverím, neverím svetu, jeho lákadlám, jeho ponukám.

7 7

Neverím, neverím srdcu, srdcu človeka, stále uniká. R:

2. Nevidím, nevidím šťastie, iba zdanlivé, veľmi prchavé.
Nevidím, nevidím lásku, lásku trvalú, ktorá dáva. R:

3. Široká, široká cesta, cesta človeka, ľudských múdrosti,
ponúka, k slobode zve ťa, ktorá zotročí, vedie do smrti. R:

Ó, Pomocnica, Ty Panna čistá, Ty hviezda jasná

(őcG c_ G g' DgfCcGcC _ _ Gg7pg' _QcGc
1. , Pomocnica, Ty Panna čistá, Ty hviezda jasná si nášho žiti-a.

   

C c G c_ FfAfG g' D g'  cGc _G gDg_ _  cGc
R: |:Sme síce hriešni, však, Matka, Tvoji, ó, Pomocnica, stoj pri nás v boji.:|

2. Za búrky chráň nás, Ty Panna čistá, tých, ktorí Tabe ctia si doista. R:
3. O, Pomocnica, bud' nádejou nám, útechou našou Ty vždycky zostaň. R:

