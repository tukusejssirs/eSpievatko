 

Tomáš (Počúvaj, Tomáš, videli sme Pána) hudba a text: Ondrej Studenec
F _ d G_ _ C F d _ G  _
1. Počúvaj, Tomáš, videli sme Pána, keď sme sa skryli v strachu pred idmi,
F _ _ F c d_ c F d G _c 0° c N
pozdravil "Pokoj Vám" a keď to riekol, ukázal nám ruky i bok. Tomáš, brat nas.

2. Počúvaj, Tomáš, čo vravel Majster náš, "za tri dni tento chrám postavím",
tak ako sľúbil, tak sa aj stalo, postavil najlepší chrám na zemi. Tomáš, brat náš.

3. Počúvaj, Tomáš, dôveruj Pánovi, ostávaj vo viere a v nádejí,
buď s nami ďalej a nebráň tým, ktorí už veria a nevideli. Tomáš, brat náš.

Tou cestou takou známou pôjdeme hlásat' dobrú správu text: Ondrej Studenec

   

E c!! E A' H' A' E A H'
R: |:Tou cestou takou známou pôjdeme hlásať dobrú správu.
A c** E A' E H' E A'
Tou cestou takou známou, že prišiel Ježiš k nám, alelujagl
7 ga E A H' g!! E A E
l. Tam v ďialke ktosi volá ma: "Aj ty, priateľ, kráčaj za mnou",
H7 g* E A E H' E A'

tam zloba páliť prestala, tam pieseň zneje tmou, aleluja. R:

2. Verte, blúdili sme cestou zlou, ten pútnik s nami šiel,
keď prosili sme o pomoc, On ukázal nám smer, aleluja. R:

 

Tŕňová koruna na hlave a šarlátový plášťé hudba: Fero Horváth. text: M. Fasterová
e h f!! G D

I. Trňová koruna na hlave a šarlatovy plašť,
e h ft* G _ H H
korbače čerstvo krvave, tak hadaj, Mesiaš, kto z nas Ta udrel?

C D G C D G

Aká koruna, taký kráľ, pľujú mu na oslavu,
C D D C he
zbitého Pilát ukázal rozzúrenému davu - ajhľa človek!

G _ e _ c _ D _ _ c G _ e _ co G
Na miesto súcitu dav kričí - ukrižuj Ho! Nenávisť opitú nezastavíš,
G e C D C G e C D G A“

hoci je nevinný, dav kričí - ukrižuj Ho! na pokyn menšiny: preč s Ním! Na kríž!

2. A niekto ruky si umýva a niekto seje zášť,

a niekto radšej sa nedíva, tak povedz, Pane náš, kto z nás Ťa udrel ?
Doba je opäť rovnaká, pľujú na Tvoju hlavu,

ľahko sa dáme vyľakať a pridáme sa k davu, ajhľa ľudia...

A miesto veríme, mlčíme prestrašení, a možno myslíme, že pochopíš.
A hoci veríme, mlčíme ako nemí, na seba myslíme: pokoj, nie kríž.

3. Tŕňová koruna na hlave a šarlátový plášť,
korbáče čerstvo krvavé, tak hýb sa Mesiáš, Golgota čaká...

Tu pri tvojich nohách, Pane môj, Ty vieš, sedávam tak rádtdba a text: Jiří Černý

 

o _ __ e G _ D_ e G o
Tu pri tvojich nohách, Pane môj, Ty vieš, sedávam tak rád.
e G D

e
Tu pri tvojich nohách, Pane môj, Ty vieš, rád Ťa počúvam.

