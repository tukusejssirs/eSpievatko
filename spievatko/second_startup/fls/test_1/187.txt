A D E A D E A

.° Som žitia pútnikom, nechodím sám. Pán Ježiš vedie ma v žitia Kanán.

. Mám žitia sprievodcu, Pán Ježiš sám, vedie ma na ceste v žitia Kanán.

K spánku sa ukladám V večerný čas, sní sa mi krásny sen, vidím Kanán. R:

 

Klopanie (Na dvere ktosi klope nám)

D e' _ D eD G _ A'

. Na dvere ktosi klope nám. Na dvere ktosi klope nám.
D D7 Ft* _ h o e' _ D
Načo váhaš, otváraj dvere, na dvere ktosi klope nám.

text: Ondrej Studenec

e' D e' D F1* A

D G D G A
: To klope Ježiš, na dvere stále klope nám. To je Ježiš, na dvere stále klope nám.

D D' F3* h D e' D e'
Načo váhaš, otvor mu dvere, na dvere stále klope nám.

. Od dverí kľúč máš iba ty. Od dverí kľúč máš iba ty.

Načo váhaš, otváraj dvere, od dverí kľúč máš iba ty. R:
Album: Céčka: Nebojínie sa

 

hudba a text; Richard Čanaky

Koleda (Kresťania zvestujme celému svetu)

C _ a _ F G C a _ _ D _ G

. Kresťania zvestujme celému svetu, že dnes k nám prišiel Spasiteľ Pán.
a _ e F c F c G c
Mária, panna zverená Bohu, uzrela prvá, čo dal Boh nám.
F C G C F C G C

: Ježišu, Ježišu, od Boha daný nám, skrz Pannu Máriu prišiel si k nám,

Ježišu, Ježišu, od Boha daný nám, u ľudí, v ich srdiečkach, vytvor si chrám.

. Pozrite, ľudkovia, čo je to za zázrak, ved' to sám Spasiteľ sveta tu je.

Tíško tu v náručí na svet sa prediera a s láskou za ľudí sa obetuje. R:

. Raduj sa, Boží ľud, tomuto dňu, ododnes ráta sa pre ľudí čas.

Písmo nám vraví pravdu presvätú, že Pán tak jak sľúbil, tak vzkriesi nás. R:
Album: Kompromis: Právo na lásku

hudba a text: Jozef Halmo

 

Kráľovná nebies, Panna Mária

C d G' C e F G' C
. Kráľovná nebies, Panna Mária, tys' najkrajšia z hviezd, čistá ľalia.
e F C d G' C
Ku Tebe vzdychá zbožný národ Tvoj, modlitba tichá zmierni nepokoj.
G G' C G' C
Pritúľ nás do svojho lona, k Tebe vinieme sa s dôverou,
G C G D G G'
a ked' telo naše skoná, poslednou bud' oporou.
C C' F G' C

.° |:K nám sa obráť, ó, Matka láskavá, znie naša pieseň, naša oslava.
7

C C F C d
U Syna svojho za nás oroduj, našu drahú vlasť, Matka, ochraňuj.:|

G7

. Patrónka naša, Matka jediná, k Tebe sa vznáša prosba úprimná.

Všetci sme Tvoji, vieru našu stráž, v duchovnom boji pomoc preukáž.
Národ Tvoj Ťa o to prosí, Ty si Matka dobrá, láskavá,
čo si dietky v srdcí nosí, pred zlobou ich zastáva. R:

