Hosanna (Zástup spieva, volá hosanna) text: Ondrej Studenec
h G h G A

D F9
R: Zástup spieva, volá hosanna, Kristus Kráľ sám ku nám prichádza,
G _ H' A Gh _ G _ h
každý palmami máva, Spasiteľ, pokoj, lásku dušiam rozdáva.
A D Fi* h A _ D Fi* h G 'v
1. Večer a posledná večera. v tichu naša spása dozrieva, jedna záhrada Judas a Zrada... R:
2. Cesta, ktorá život podrazí, zloba trápi dobro na kríži, nevidí, Ježiš víťazi... R:

Hviezdička betlemská, kde si    
a a' C a a' G

Hviezdička betlemská, kde si? Putujem cez hory, lesy, Ježiška hľadám.
d G' c d G' c F G ç
Poklady nenašli by ste, srdce len chudobné, čisté |zdojasieľ vkladam:|

 

Iba Ty, iba Ty Ježiš, Ty si Boh presvätý

D DW" G g D h e A7
R: Iba Ty, iba Ty Ježiš, Ty si Boh presvätý, svojou slávou ma ctíš.
o om" G g o h e A7 D

Iba Ty, iba Ty, v moci a v sláve odiaty, Ježiš sám, Syn Boží, si to Ty.

G f** h G h

1. Ked” vchádzam úzkou bránou za Tebou, Ty ma tiahneš Tvojou láskou.
G ft* h e e_' _ A A5'

Ked' kráčam úzkou cestou za Tebou, Ty ma vedieš, Sl môj Pán. R:

2. Ked' dávam túžby svoje na oltár, Ty sa skláňaš, plnosť dávaš.
Umieram Tebe, Pane, každý deň, si môj život, si môj Pán. R:

 

Ideme smelo k výšinám, chceš s nami
E H7 _ E(A) H'_(E) _ w _ i E(A) H'_(E)
1. Ideme smelo k výšmám, |:chceš s nam1?:| Velebiť Boha v nebies chrám, |:chceš s nam1?:|
Zástupy šli už pred nami, hriechov a útrap zbaveni. Odišli v kríža znamení, |:chceš s nami?:|

2. My cestou šťastia kráčame, |zpoď s na1ni,:| Radostným srdcom Voláme, lzpod' s nami,:|
S hriechom sa navždy rozchádzam, Kristovi srdce oddávam, Jemu sa v náruč priviniem, |:už idem,:|

3. Hľa, Matku dal nám nebies Pán, |:pre všetkých,:| Počúvaj vždy Jej nežný hlas, |:nezblúdiš,:|
Bude Ti svietiť na cestu, do raja ku blažených mestu. Ved' Mária, Božia Matka,je nádej nám sladká.

 

Idú traja králi nočnou tmou hudba a text: Ondrej Studenec
H7 E s n  E a o  u
1. Idú traja králi nočnou tmou, milióny hviezd,
E A' H' E H'EAEHAEH

len jedna práve nad hlavou, ňou nechali sa viesť.
2. No a tam na slame v jasliach v Betléme, malé dieťa spí,
E 7 _ H' E A c* E A' E Al' H'
že kráľom kráľov kraľuje, viem to dobre ja aj ty. láske, pokoju nás naučí, uvadaptap tudup.

3. A ľudia dobrej vôle spievajú s anjelmi glória, až srdce poteší
a šťastná je aj Mária, dieťa drží V náručí, láske, pokoju nás naučí, uvadaptap tudup.

4. Idú traja králi nočnou tmou, milióny hviezd, len jedna práve nad hlavou, ňou nechali sa viesť.

