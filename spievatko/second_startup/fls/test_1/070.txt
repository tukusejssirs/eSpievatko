5. Pretože dávaš nám Život. 7. Pretože môžeme spievať.
6. Pretože stále si s nami. 8. Preto bud' s nami na veky.

   

Verím V Boha, Otca všemohúceho, Stvoriteľa neba i zeme
G a' G CD'G e a' D'

Verím V Boha, Otca všemohúceho, Stvoriteľa neba i zeme,

G a' G C G

i V Ježiša Krista, Syna Jeho jediného, nášho Pána,

e a' D' G efWGW ci* c*** ft" H'

ktorý sa počal z Duchu Svätého, narodil sa z Márie Panny,

ci* cit' ft' H' ci* a gťcüñ' H'

trpel za vlády Pontia Piláta, bol ukrižovaný, umrel a bol pochovaný,

cti cti" 7 7 c! cW

zostúpil k zosnulým, tretieho dňa vstal z mŕtvych, vstúpil na nebesía,

fl“ H' cl* a llcllfll' H' D'

sedí po pravici Boha Otca všemohúceho, odtiaľ príde súdiť živých i mŕtvych.

G a' GCG e a'D' G a'

Verím v Ducha Svätého, v svätú cirkev všeobecnú, v spoločenstvo svätých,

G C G e a' D' G ea'D'G
v odpustenie hriechov, vo vzkriesenie tela a v život večný. A--men.
text: P. Malý Ě

   

Viem, že čosi nám chýba, ked' sa na oblaky dívam

AcťD E _ _ Act* D E Act* D
1. Viem, že ČOSI nám chýba, ked' sa na oblaky dí--vam,
E A D E

krásu nejak neprežívam, aj ked' purpur hrá.
fl*

Keď spievame pieseň, celkom hluchá je, cítim, duté, bezradné slová má
D  DAE A C3D EAcl*
a tóny sama jeseň sú zaschnuté v nás.

2. Niekedy to tak býva, stojíme tu starí známi,
každý má najlepšie plány, každý svoje vie sám.
Stávame tu tak často, ked' sa čítava Písmo, aj tak nevieme spolu rásť
a sme na seba ticho snád' pre pýchu len.

3. Viem, že čosi nám chýba, ked' máme veľa práce,
ked' sa nedá odpočívať, ked' nevieme, kam skôr.
Možno dajú nám úspech, no už vzali si úsmev, často dusí nás zlosť na zlo
a každodenná starosť v nás pochová deň.

4. Viem, že radosť nám chýba, radosť, ktorá všetko dáva,
iskra, čo nás prekonáva, svetlo pre celú zem.

|:To je radosť s Kristom, toto je radosť z lásky,
(fľEA)
to je vtedy, keď Boh je náš, náš Otecko a úsmev náš pre celú zem.:|

    

Viera, tá Ťa uzdraví a všetky rany zhojí
G D' _ G ' _ D_' _ (G,C) G
R: |:Viera, tá Ťa uzdraví a všetky rany zhojí, viera, tá Ťa uzdravi, jak slnko žiari tmou.
G D G _ EE( _ G Dj _ GC

. Zo zbabelosti ľudskej, z pocitov nestálych, z mámosti sebaistej, priateľov byvalych.

. Len viera, tá Ťa chráni, ked' nenachádzaš kľud, daj srdce, čo Ti brání, len ver, zbavíš sa pút.
. Za ruku slepých vedie a sily násobí, na duše ubolené jak balzam posôbí.

Uul\)&.

