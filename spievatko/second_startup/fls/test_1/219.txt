Ě Túžba (Zem už nech je mier) s

e CW" e a e a G D

1. Zem už nech je mier, nech je rajská záhrada, nech sa vráti sen do ľudských sŕdc.
e C'"'" e a e a e C e
Pomníky bez mien nech sa navždy pominú, nech je slnko deň a hviezdy pre tmu.

D A a _ e D A a e DAa e DAa e
Coda: Kríž nech je len spásou, kríž bez nových rán, tíš pre poľných vtákov, tíš pre Boží chrám.
2. Človek nech má chlieb, v rukách býva láskavý, nech vchádza vždy brat do našich brán.
Kto má ešte viac, nech sa s plášťom rozdelí, nech už nestretám žobrákov tvár. C:
G _ e D a _ e G _ D _ C e
R: Z najväčších pút, z najväčších síl, z najväčších slov je Láska.
G__e D_ a__ e __ h'_a'e
Z najkrajších ciest, z najkrajších dní, z najkrajších snovje Láska.

3. Šťastie nech je dar, nech sme pýchy zbavení, pravda vyčistí zákemý súd.
Nech V nás žije Pán, v mene lásky zídených, nech dá všetkým sám kráľovský kľúč. C: R:

 

Ty si mojou skrýšou, ja skrývam sa v Tvojich rukách i i hudba: Michael Ledner

ic» . .9 „ .c , Fe" .. B,
Ty Sl mojou skrysou, ja skryvam sa v Tvojich rukach,

7

g d
chrániš ma, Pane, ked' príde strach ma objímeš, verím len Tebe.
9 C F . B 9. . A' A'
Ja verím Tebe, dnes poviem Tebe: Ty si silou mojou, stráž moja.

   

Ty vieš, že chcem, Pane môj, bojovať ten dobrý boj

C _ a d E _ a _ D' G _
1. Ty vieš, že chcem, Pane môj, bojovať ten dobrý boj,
C E 7 C d G C

Kým som mladý, plný síl, rád by som Ti ku cti žil.

2. Svetom vládne hnev a svár, pravá láska vzácny dar!
Ty si láska, vravíš nám, blížnych k tebe zavolám.

3. Na žiaľ zem je bohatá, slzy nik tu nezráta. Kde je žriedlo sily, viem, ľudom o ňom rozpoviem.

4. Kým som mladý, Pane môj, bojovať chcem dobrý boj.
Len si múdrosť, silu daj, Duchom svojím pomáhaj.

  

text: Ondrej Studenec

  

Už bolo blúdenia dosť, pozri, prichádzam za Tebou

 

A' o h _ E _ A o O _ h E od“
1. Už bolo blúdenia dosť, pozri, prichádzam za Tebou plniť Tvoju vôľu chcem,
D A G D h A7 D A'

ukáž mi, Pane, cestu, pôjdem ňou s pokorou, bez Teba ju nenájdem.
DD""h E” Ce°_A7
R: So srdcom úpriinným hľadáme Tvoju tvár,
_ DD5*hE°Ce°A'
do svojho svätého chrámu príď, ó, Pane, na oltár.
2. Prichádzam, Pane, lebo chcem, lebo viem, že to chceš aj Ty, pri Tebe mám viacej síl,
síl nech sa zapriem, vezmem na plecia svoj kríž, len s Tebou to vydržím. R:

3. Nepokoj, keď sa ku mne vkradne, prosím, Pane, upokoj, si v mojom srdci vítaný,
môj smäd po pravde uhasiť si pomohol, oporou vždy Ty si mi. R:

