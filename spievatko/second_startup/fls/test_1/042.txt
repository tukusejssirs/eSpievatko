 

Slnko, náš brat, prinášaš krásny deň hudba: Donovan

c e _F G c a e F G c

1. Slnko náš brat, prinášaš krásny deň, lenže tak málo počúvam tvoj hlas
a e F G C
a ten hlas má moc prebúdzať dobro v nás.

2. Vietor snáď tiež povedať mi chce, ako si s láskou stvoril tento svet,
svet pre ľudí a voľný vtáčí let.

F _ e_ d C C a e F G
R: Som Božie dieťa a cítim, že chcem počuť Tvoj hlas, čo posielaš na zem.

3. Hovoríš k nám cez svetlo mesiaca a lúče slnka, modrú oblohu.
Len keď mám rád, tak všetko pochopím.

 

Prichádza čas, ked' už svet počuje ten hlas hudba a text: L.Richie a M.Jackson
E _ , A H E A H E
1. Prichádza čas, keď už svet počuje ten hlas, ty a ja, vieme, jedno je v nás.

cl* g** A H
Tam svet čaká ľudí, ó, je čas život žiť, veď On je Ten, Ten najväčší dar.
2. Nedá sa ísť a predstierať každý deň, že raz tak ktosi zmení náš svet.
My všetci sme z Božej obrovskej rodiny, pravdou je, vieš láskaje ten liek.

A H E A H E
R: Všetci sme svet, ú, ú, všetci sme deti. Všetci sme to, čo robí krajšim deň, Začnime dávať.

cl* git
Je tu veľká šanca rozdať sa pre druhých, urobiť lepším dnešný deň, len ty a ja.

3. Vedieť im daj, že ich predsa má niekto rád. Sila a nádej bude V nich rásť.
Boh nám lásku dáva a dal chlieb keď bol hlad a teraz chce ho ľuďom dať cez nás.
_ c o _ _ E _ c D E
Coda: Keď si na dne a sám a nádej ti uniká. Ale ty len ver, padnúť nemáš kam.
cl* g** H
Dokážme všetkým, zmeniť sa všetko dá, musíme chcieť, stále Ty a ja.

Hlava, ramená, kolená, palce
D

A D(A,D) o A o A _ _ D
Hlava, ramená, |:kolená, palce,:| Hlava, ramená, kolená, palce, oči, uši, ústa, nos.
Pokojne do ticha modlitba znie m

CGC F c G alEC _d _cGc_
1. Pokojne do ticha modlitba znie, že Pán je tu s nami, nad nami stále bdie.

2. Keď Mária do Jeho rúk sa vložila, tak bez slov, s dôverou k Nemu sa modlila.
3. Ó, Pane, tvoja vôľa nech sa stane, my s Matkou v dôvere z vďaky Ti spievame.

Prišiel na zem konať dielo spásy m
C G' C E' d

a
1. Prišiel na zem konať dielo spásy a ľuďom biednym s láskou pomáhal,
C G

7

ku slobode zval zotročených hriechom, môj Pán.

Svetom šiel a ľudskú niesol biedu, chcel z lásky splniť Otcov spásny plán,
na kríži mrel, však tretí deň vstal z hrobu, môj Pán.

