o, tak dobrý je náš Pán (nezabudni, čo pre teba vykonal)

A E7 A

1. O-ó-ó, tak dobrý je náš Pán, ó-ó-ó, tak dobrý je náš Pán,
A7 D A“'"' A D E'A
ó-ó-ó, tak dobrý je náš Pán, nezabudni, čo pre teba vykonal.

2. On pomáha ti stále, tak dobrý je náš Pán, On dáva novú nádej, tak dobrý je náš Pán,
On pomáha ti stále, tak dobrý je náš Pán, nezabudni, čo pre teba vykonal.

m Pane obnov v nás ten Tvoj prvý hlas
C d7 G7 Cmaľl C6 d? G7 cmaj7
1. Pane obnov v nás ten Tvoj prvý hlas. Denne chceme počúvať Ťa ako prvý ráz.
C I dv G7 . Cman Cs d1. G7 C C7
Pane obnov v nás prvej lásky čas. Denne chceme milovať Ťa ako prvý raz.
7 Fmaľl f; cmaj7 Fmaj? ř e7 ej] d1
R: Ty si ten živý Boh dnes ako včera, živé slovo máš. U Teba láskaje a živá viera, nové srdce dáš.
2. Pan obnov nás, milosti je čas. Denne chceme chváliť Teba a vo viere rásť.
Pane; obnov nás, zrelý je už klas. Denne chceme slúžiť Tebe, je tu žatvy čas. R:
Pán Ježiš je Priateľ môj, nebudem sa báť „
A h' D A A h' D A

 

1. Pán Ježiš je Priateľ môj, nebudem sa báť, každému chcem povedať, ako ma má rád.
h' A

V Ňom je pravej lásky zdroj, miluje nás všetkých.
A h'

Stačí mu len povedať: "Chcem, buď Priateľ môj."

AI» _ Eb L Db EF
R: Je vzor všetkej skromnosti, ved' v Nom sa zjavil Boh Otec sám,

Ab Ek f Dl» El»

prišiel k nám v jednoduchosti, a predsa je najväčší Kráľ.

A|› EB f Db Elv

Neváhal, život za nás dal, aby hriešnik spásu mal.

Ab Elv f DF EF C D

Na tretí deň zmŕtvych vstal, a tak dielo spásy dokonal.

2. Zanechal nám posolstvo ako treba žiť, že si treba blížneho ako seba ctiť.
Ten, kto v Neho uverí, bude spasený, tak to vravel Pán Ježiš, ked' žil na zemi. R:

3. Pán Ježiš je Priateľ môj, nemusíš sa báť, chcem ti tým len povedať, že ťa má veľmi rád.
V Nom je pravej lásky zdroj, miluje ťa vrúcne, s celou svojou vrúcnosťou chce byť Priateľ tvoj.

text: Marcel Šišković

   

k nám, vravel to sám

 

Pán má prísť
C

G' C G C
1. Pán má prísť k nám, vravel to sám, nejeden dôkladne spozná, že jeho cesta je hrozná.
7 F G C
Prestane pánom kvitnúť, viečka sotva sa od hanby zdvihnú,

G C G C
ľútosť naveky hodnotu stratí, súd je definitívne platný. Neodvoláš, nezmeníš.

2. Pán má prísť k nám, vravel to sám, nejeden až potom to zistí, že jeho život je čistý.

Problémov Pán ho zbaví, nájde pokoj a priateľov pravých.
Viem, že o tom, čo prichystal Kristus, máme predstavu naozaj hmlistú. Veď sa dočkáš, uvidíš.

