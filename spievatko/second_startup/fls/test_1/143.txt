2. Duch Boží napĺňa milosťou každého, kto príde s radosťou.
Kostol premieňa sa V Boží dom, ak sú otvorené srdcia V ňom. R:

m Celý život chcem spievať Tebe, Pane hudba a text: Mária viznerová
C e C_' F _ C d g Ce dG
1. Celý život chcem spievať Tebe, Pane, ospevovať budem a pokiaľ žijem.
c e c' _ F _ _ _ c_ _ d G7 c F
Na Teba myslieť, Pane, je najmilšie, radujme sa, veseľme sa v Pánovi.
C d _G _Ca d G' C C'(FC)
R: lzChváľ Pána duša moja, haleluja, chváľ Pána duša moja, haleluja.:-/

   

V

2. Celý život chcem slúžiť Tebe, Pane, počúvať Tvoj hlas budem pokiaľ zijem.
V živú obeť Ti dávam seba, Pane, na oltár Tvoj dávam sa posväť ma. R:

Ě
D e(A) DA' D e(A) D

1. Cesta, |zďaleká cesta,:| ktorou i-dem. Dávno, lzzmizla už dávno,:| moja nádej.

2. Za kým, |:za kým však pôjdem,:|, bez nádeje? Ježiš, |:Onje tá cesta,:| On volá ťa.

3. Ježiš, |:On je tá láska,:| On chce ťa nájsť. Poďte, |:s Ježišom poďte,:| kým volá vás.
Chválim a zvelebujem Ťa, môj Pane

G_C'_ _Gc=G_c= GC'
Chválim a zvelebujem Ťa, môj Pane, chválim a zvelebujem Ťa, môj Pane.
2

 

e C a G G
Chválim a zvelebujem, chválim a zvelebujem, chválim a zvelebujem Ťa, môj Pane.

Chválim Ťa,
A fl D E
|:Chválim Ťa, môj Pane, chválim Ťa, môj Pane, chválim, Ty si môj Pán.:|
|:Ty si môj Pán, Ty si môj Kráľ, Ty si môj Pán, Ty si môj Kráľ.:|
a Chválim Ťa, Pane... chválim za všetko dobro hudba a text: Félix mac

G C' (D) (C)
1 . |:Chválim Ťa, Pane, chválim, chválim Ťa, Pane, chválim za všetko dob-ro, za Tvoju lásku.:|

   

môj Pane... chválim, Ty si môj Pán

 

C C' G
že si zhliadol na mňa, veď ktože som ja dal si mi svojho Ducha, ktorý volá "Abba".
G C'
2. Chválim Ťa, Pane, chválim, chválim Ťa, Pane, chválim za všetko dobro.

Ďakujem Ti, že si ma miloval dávno predtým než som prišiel na svet,

ďakujem Ti, že som sa počal z lásky mojich rodičov,

ďakujem Ti, že si sa mi dal spoznať v bolesti ale i v radosti,

ďakujem Ti, že si mi dal bratov, s ktorými Ťa môžem chváliť a oslavovať,
ďakujem Ti za dar kňažstva, že môžem ohlasovať Tvoje slovo po celom svete.

Že si zhliadol na mňa, ved' ktože som ja, dal si mi svojho Ducha, ktorý volá "Abba".

Chváľte Hospodina všetky národy,
d d' _G 8"" c a _ d _
|:Chváľte Hospodina všetky národy, velebte Ho všetci ľudia.:|

velebte ho všetci ľudia

   

a d a d
|:Lebo je preveliká nad nami milosť Jeho a pravda Boha naveky. Aleluja.:|

