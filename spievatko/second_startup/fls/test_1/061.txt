m Našiel som cestu do radosti, našiel som cestu do raja

_ c F c _ e _ a o G G7
1. Našiel som cestu do radosti, našiel som cestu do raja.
C F C C G C

Po tejto ceste do večnosti kráčame spolu ty a ja.

F C e aDG C e F D CG C
R: |:Aleluja, alelu-ja, alelu-alelu-alelu-alelu-alelu--ja.:|

2. Pán Ježiš Kristus, Priateľ verný, je Cesta, Pravda, Život sám.
S Ním z hriechov všetkých očistení kráčame spolu k výšinám. R:

2. Nad nami slnko lásky svieti, veď my už nie sme každý sám.
Ideme spolu úzko spätí, cestu nám ukazuje Pán. R:

3. O radosť nás nik neoberie, keď žije opravdivo v nás.
Korení v nádejí a viere, že uvidíme Božíjas. R:

4. Nesieme svetu svetlo viery, nesieme lásky zázrak sám.
S radosťou stretneme sa v cieli, kde nás čaká náš dobrý Pán. R:

 

m Náš Pán je tu s nami, máme radost' v srdci hudba a text: PeI/egrinni & Merriment
c _ d _ F _ c; _
Nas Pan je tu s nami, máme radosť v SľdCl a nie sme sami.
C d F G C d D G

Ty, spievaj s nami, nebuď smutný a teš sa s nami. Spievaj a tlieskaj, že Ježiš je náš Pán.

m Ochotný som všetko stratiť, len lásku nie
D " A A' D

1. Ochotný som všetko stratiť, len lásku nie. Vlastný život v dare skrátiť, len lásku nie.
D7 G D A A'

VV'

Ochotný som všetko stratiť, len lásku nie, najväcsi poklad je lásku mať.

2. Svet mi núka všetko, Pane, len lásku nie. Svet mi vezme všetko,Pane, len lásku nie.
Svet mi núka všetko, Pane, len lásku nie, najväčší poklad je lásku mať.

3. Bez radosti zmiera viera, len láska nie. Kto má lásku, neumiera, to láska vie.
Bez radosti zmiera viera, len láska nie, najväčší poklad je lásku mať.

4. Ty si, Pane, lásky prameň, chcem lásku mať. Moje srdce svojím zameň, chcem lásku mať.
Ty si, Pane, lásky prameň, chcem lásku mať, |:najväčší poklad je lásku mať.:|(5x)

On je kráľov Kráľ, On je pánov Pán

   

D G D A D D G D A D g A h e D ' A D'
R: On je kráľov Kráľ, On je pánov Pán. o má zmysel, s ním sa končí, s mm sa začína.
D A D e' D A D

1. Ten, kto je hriešny, smie mu veriť, on je veľký Kráľ.
A rozhodnúť, či s ním chce prežiť, on je veľký Kráľ.

2. On musel svoj život za nás dať, on je veľký Kráľ.
A nášho Boha dal nám poznať, on je veľký Kráľ.

3. Ja slávim Boha, Syna jeho, on je veľký Kráľ.
Krista, Pána vzkrieseného, on je veľký Kráľ.

