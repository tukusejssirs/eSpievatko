5.

LQIQF\

 

E A E H E
. Vždy, keď vstávam, Ty ma stále biedneho zobúdzaš, svojím dychom Ty osviežiš
H7

 

D
: |:A tu začala sláva Máriiných dní, keď svojho Syna Boh dáva nám, hoci sme zlí.

C G
. Jeden deň je V roku, keď sa všetci v jedno spoja, spoločne na zázrak čakajú.

hudba a text: Jozef Ha/mo

A_ E _ H
tiež moju tvár.

Krásny deň (Vždy, ked' vstávam)

c** A
Dobré skutky z lásky Ti dám, vždy Ti poviem - mám Ťa rád.

E cl* A
.' Krásny deň, Ty ponúkaš mi, Pane, každý deň, chcem radosť všetkým rozdať v tento deň,

E c**
byť priateľom v každej chvíli. Krásny deň, Ty ponúkaš mi, Pane, každý deň,
H A H E
chcem radosť všetkým rozdať v tento deň, žiť, rásť, milovať každý deň.

. Chválu Ti vzdám za pokrm, čo dávaš nám, sýtiš ma rád, v Tebe stály zdroj lásky mám.

Chcem vždy plniť len Tvoju vôľu, za všetko Ti chválu vzdám.

.' Krásny deň, je veľký dar od Teba, ja to viem, chcem ďakovať Ti, Pane, každý deň,

za lásky dar Tebe, Pane. Krásny deň, je veľký dar od Teba, ja to viem,
chcem ďakovať Ti, Pane, každý deň, že ja vôbec som, ďakujem.

.° Krásny deň, je veľký dar od Teba, ja to viem, chcem ďakovať Ti, Pane, každý deň,

|:žeja vôbec som, ďakujem.:|
Album: Dominik: Ty a Boží svet

 

Krásny je deň, krásna je stráň, more

c cm-vc' F G c F E' F G'

. Krásny je deň, krásna je stráň, more, ktoré stvoril Otec, náš Boh.
. Krásny je strom, zdravé plody v ňom, príroda, čo stvoril Otec, náš Boh.
. Nad všetko je Pán, nebeský náš Kráľ, stvoril nebo, vtáctvo, Otec, náš Boh.

Album: Radostná srdce: Úzka brána
text: Marcel Šišković

Krásny plán mal s nami Pán

G C
. Krásny plán mal s nami Pán, kým sme v sebe niesli život svieži,
D G
keď tvoril s láskou ten svet nádherný.

Rozhodol to celkom sám: v správnom čase príde na svet Ježiš a nádejou ho usmemí.

G

D' G D D'(D) (G)

A slovo telom sa stáva: Boh nekonečný o tomto tajomstve Pána znie spev anjelskýgl

. Láskavý je ozaj Pán tým, že ľuďom dáva ženu Matku, ked' spasiť túži ten svet zblúdený.

Rozhodol, že cez ňu k nám príde ten, čo dá mu za svet splátku a víťazstvom ho premení.

 

Kristus Kráľ (Jeden deň je v roku)

CG

Vtedy všetky detí sveta pri obloku stoja, na oblohe hviezdu hľadajú.
G C

G C F
: |:Kristus Kráľ, príde k nám, v nebi spievajú a my sa tešme.

G c F G c(G)(ç)
Kristus Kráľ, príde k nám, v nebi spievajú Gló-r1a.:|
|:Kristus Kráľ, príde k nám, pokoj, lásku rozdá nám.:|(4x)

