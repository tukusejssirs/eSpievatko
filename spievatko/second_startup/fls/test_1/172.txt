 

V srdci svojom túžbu skrytú mám

d e' FW" e d G

l. V srdci svojom túžbu skrytú mám: byť raz takým, Pane, jak ty sám.
C d e FW" e F e a
Pane, ty si majster, majster môj, môj veľký vzor, všetkej pravdy zdroj,
e a d' G C dG

vždy pri mne stoj, môj Pán, chcel by som sa tebe podobať.

C d e F'“*'" e d G C d e F'“°" ea
2. Ja v srdci túžbu mám, mať lásku, jak ty máš, ja túžim s tebou rásť,
e a e a d G C dG
veľký vzor, pravdy zdroj, pri mne stoj, chcel by som sa tebe podobať.

3. Vtedy, keď si blízko, nie som sám, vtedy všetko isté, jasné mám,
prečo treba dávať, rozdávať, vždy pevne stáť a neváhať,
aj z mála dať, môj Pán, tak ako ty milovať.

4. Ja túžim s tebou rásť, mať lásku, jak ty máš. .la túžim s tebou rásť,
pevne stáť, neváhať, z mála dať, tak ako ty milovať.

Veselej vianočnej doby, spievajte bratia koledy

__FC_G'C a_e_ F_CGC
1. Veselej vianočnej doby, spievajte bratia koledy.
C G C G C G'C

|:O tom, čo sa v noci stalo, že sa ľuďom narodilo dieťatko.:|

2. Dnes sa Ježiš Kristus narodil, aby nás z hriechu oslobodil.
lzPoďme, bratia, nemeškajme, Ježiškovi sa klaňajme malému.:|

3. Ježiško náš drahý, zrodený, svet Ťa zvelebuje spasený.
|:Božie, malé, drahé dieťa, spievame ti, prosíme ťa, žehnaj nás.:|

 

V knihe kníh, večnej zvesti žitia napisal svedok zopár slov

G _ h _ c _ D_ _ G c
1. V knihe kníh, večnej zvesti žitia napísal svedok zopár slov,
G D G C
ako Pán z dvoch rýb, piatich chlebov dal najesť hladným zástupom.
Ě _ e _ _ D h _ _ e C D
R: akali hojnosť, čakali mannu, ty Sl im telo a krv ponúkal,
_ e __ C _ D h _ e C _ D
nechceli prijať tvoju reč tvrdú, nám Sl však víno a chlieb zanechal.

2. Chceli Ťa urobiť si kráľom, chceli mať chleba navždy dosť,
ty si však utiahol nabok, prosiť za svojich rybárov.

3. Rozdával si im chlieb a ryby, rozdával si sa ľuďom sám.
Teraz prosíme: Pane, prídi cez ruky kňaza na oltár.

Vďaka za toto krásne ráno, vďaka za každý nový deňvudba a text: M. S. Schneider

   

D e fl* D G A
I. Vďaka za toto krásne ráno, vďaka za každý nový deň.
D D' G G“'"' D A D

Vďaka za to, čo už je za mnou ako ťažký sen.

2. Vďaka za všetkých verných druhov, vďaka, ó, Pane, za tvoj ľud.
Vďaka, že radi odpúšťame urážky aj blud.

