E Duša moja, oslavuj Pána, oslavuj Pána
a G F E a G

FGA

R: Duša moja, oslavuj Pána, oslavuj Pána, oslavuj Pána. Duša moja, oslavuj Pána, a-leluja.

C C' F C G
1. Pane Bože môj, aký si len veľký, zaodetý do veleby, krásy.
C a F G A GEAA'
Spevňuješ Zem, dal si jej základy, pevná stojí po celé veky

D D' G D
Modrými moriami ju napájaš, prameňom rozkážeš narásť v rieky.
D fl* h G A H C D E

Napájaš všetko živé na Zemi, V korunách stromov spievajú vtáci. Aleluja.

1324. Privítať chcem Ťa, Pane, privítať uprostred nás

A _ E _ E D
I. Pnvítať chcem Ťa, Pane, privítať uprostred nás,
h E

osláviť návrat zázračný, ohlásiť hody pre lačných.
A _ E A E A_ E D_
Privítať chcem Ťa, Pane a túžim s Tebou žiť,

E D E
nemôžem ďalej hlúpo stáť, pokús sa vo mne zmŕtvychvstať.
E _ A _ h E A
R: Skúšaj, skúšaj, prebuď nás, spíme spánkom zlým,
E A _ h E A _
príď a spánok ukonči ránom sviatočným.
2. Ľahšie Ti bolo Pane zostúpiť do chleba,
ťažšie je cestu nájsť a spôsob do sŕdc namyslených osôb.
Ľahšie Ti bolo Pane vystúpiť z tvrdých skál,
ťažšie je čosi dosiahnuť, keď skúšaš ľahostajným hnúť.
E Vzdajme vďaku nášmu Bohu, Vzdajme vďaku Pánovi

 

D _ A e G D _ A eG
|:Vzdajme vďaku nášmu Bohu, Vzdajme vďaku Pánov1.:|

A G D G
Ved' On stvoril nebesia i zem je plná Jeho slávy. On sám je mocný a láskavý.

On jediný mi rozumie, On sám mi stiera slzy. On vraví, že ma miluje, On je tým Bohom pravým.

E Dotkni sa očí mojich, Pane, túžim uvidieť Ťa
E HÚ = c' AH
Dotkni sa ocí mojich, Pane, túžim uvidieť Ťa.
Dotkni sa perí mojich, nech môžem chváliť Tvoje meno.
Dotkni sa srdca môjho Pane, obnoovuj.

 

A A H
Zošli svojho Ducha, aby očistil ma. Zošli svojho Ducha, aby uzdravil ma.

