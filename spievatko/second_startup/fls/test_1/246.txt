B C B C
3. Nájdeme cestu, po ktorej kráča mier. Nájdeme cestu, po ktorej i náš Pán šiel.
a e F e F G
A ja i ty sme zachránení, veď žijem ja a žiješ ty na zemi.

Nechápem, prečo my, bez lásky tu na zemi, žijeme a nechceme o nej počuť?
To sila je, ktorú má, dáva svetlo temnotám, my dúfame, že nevyhasne.
Spojme si ruky, srdcia si spojme tiež, majme jeden cieľ, dajme svetu mier.

e a e F G
4. |:Mier, ktorý všetkých spája a lásku, ktorú dáva, majme jeden smer,
F G C F G
ktorý je za mier.:| lzKtorý je za mier:| (3x)
Pozn.: Pieseň pôvodne naspieval Michael Jackson: Heal the world.
Tú nádej mám (Ak budeš hľadieť na neprávosť)
c _ G E' _ _ a F _G _ F c
1. Ak budeš hľadieť na neprávosť, Ježišu, môj Pán, kto obstojí pred Tvojou tvá-árou ?
c p E' _ _ a F _G F c
Z hlbín volám, volám a, počuj môj hlas, Pane, neobstojím pred Tebou.

a _ G _ F c a _ G F _ _ G
R: Tú nádej mám, že Sl sa sám obetoval, hriech, neprávosť na seba vzal, ty S1 môj Pán.

      

hudba: Radostná srdce

2. Milosť Tvoja veľká je, siaha až k nebu, prevýšila myseľ každého z nás.
Len jedna vec vedie ma vždy ku pokániu, dobrota Tvoja, Ježiš, Pán.

Mobil (Volám každý deň i noc) Ě
D A D e A D h

G
|:Volám každý deň i noc, príď mi Pane na pomoc.:| V mojom súžení, v mojom trápení,
e A D h G
v mojom padaní, v mojom vstávaní. |:Príď mi Pane na pomoc:| (4x)

     

Privítajme Pána (Všade tam, kde sú ľudia zídení)

G D G D G

Všade tam, kde sú, ľudia zídení, v mene Ježiš, v láske zjednotení.

a e _ c G c _ , _ a c _ D' D
Práve tam Boh zasľúbil, že bude prítomný. Dnes viem, že naš Boh je na tomto mieste prítomný.

C G D G
R: |:Privítajme Pána v tomto chráme, nech ho Božia sláva naplní,
C G D G
svojmu Bohu spievame a hráme pieseň spasenýchzl
Interpret: Tretí deň

   

Spolu s každým novým ránom, spolu s vánkom večerným

G D e C
1. Spolu s každým novým» ránom, spolu s vánkom večerným.
G D

B
Ticho stúpam k výšinám, chcem miesto nájsť, kde láska vládne nám.
D e C
Všetky rýmy krásnych básní, všetky hviezdy vzdialené,
G D G

všetky rieky púšť i vietor lásku hlásajú.

