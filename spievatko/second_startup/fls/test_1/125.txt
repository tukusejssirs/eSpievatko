m Mám pocit blaha ked' kľačím pri nej v tichu sama
C o a F o c G a
1. Mám pocit blaha, keď kľačím pri nej v tichu sama,
C a F G
keď pozerám sa do jej očú, či smejú sa a možno plačú.
C a F G
Vtedy cítim, že nie som sama, že so mnou je vždy Mária,
C a G C
vtedy cítim, že moja viera musí byť silná, aby neumrela.
C_a_F_G _C_a_F_G _C
R: |:Mária, Mária, Mária, ó, Matka Mária, Mária, Mária, Mária, Nebeská Mária.:|
2. Šepkám ticho raduj sa, raduj sa, Mária, milostí si plná až na veky, Mária.

Vtedy cítim, že nie som sama, že so mnou je vždy Mária,

vtedy cítim, že moja viera musí byť silná, aby neumrela. R:

Pozn.: Pri' opakovaní refrénu sa medzi' Nebeská Mária urobi' pauza. (Nebeská - Mária)

m Lásku dá. Čo dá. Dá šťastie, radosť, pokoj dňa. Aha
c a F G _ c _ a F
1. Lásku dá, "Čo dá?" dá šťastie, radosť, pokoj dňa. "Aha!"
_G _ C_ a F G _  FG
Miluj ho a UVldĺŠ, že "Že čo?" že Boh ťa miluje tiež.

   

2. Vyber smer, "Aký siner?" ten, ktorý ti ukázal Pán. "Aha!"
Miluj ho a uvidíš, že "Že čo?" že Boh ťa miluje tiež.

3. Odplať mu, "Že čo?" že svoju lásku daruj mu. "Aha!"
Buď verný a uvidíš, že "Že čo?" že šťastný budeš na veky.

 

Anjeli pred trónom ti spievajú, svoj hlas k nim pridáme text: Rieka života
G__D G _c_D_G _G _D_ G c DG
1. |:Anjel1 pred trónom ti spievajú, svoj hlas k nim pridáme tebe na slávu.:|

G D G D G _ C D G
R: |:Svätý, svätý, svätý, Boh náš Pán, on bol, on je a príde zas k nám.:|

2. |:Vždy svätých chvály sú ti vzdávané, my s nimi pred trónom ich refrén spíevame.:| R:

3. |:Keď starci padajú tu pred trónom, my srdcia pokome skláňame pred tebou.:| R:

m Ako vtáčik v lese spieva slobodne, tak aj ja chcem

C _ d'/_c d _ _ G C _ FG
1. Ako Vtáčik v lese spieva slobodne, tak aj ja chcem tebe, Pane, spievať.

C ďlc d G

V tvojej láske m-ôžem spievať radostne, v tvojej láske slobodu už mám.

c _ e _ F _ d _ G'G
R: Chválim ťa za tie krídla lásky, chválim ťa, čo daroval's mi, Pane.

F

C e C F
Chválim ťa, tvoje krídla, Pane, chválim ťa |:ma vyzdvihli:| |:z hriechu a tmyzl
c _ F c d G c
|:ma vytrhlizl |:z pochmúrnych dnízl |:ma prikryliz| mojím Pánom si Ty.

2. Keď ma život zamkne do ťažkých všedných dní, otváraš mi cestu, bránu k tebe.
Na teba len hľadím, Ježišu môj príď, Ty si cesta, brána, život môj. R:

