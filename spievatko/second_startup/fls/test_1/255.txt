m Vďaka za soľ, čo svetu dáva silu hudba a text: A. F/eury, 2002

D e f! G D e A
R: Vďaka za soľ, čo svetu dáva silu a chuť životom kráčať.
G Afl* h C A GD
Láskou zjednotení vo svetle môžeme stáť, Nekonečný Pán.
F GÍF e a d' B A
1. Pravdu môžeme nájsť, keď pripojíš sa k nám, keď čítaš Písma, srdcia horia nám.
F G/F e a ď B A

keď sa lámal chlieb, tak zrazu sme ťa spoznali, no celú pravdu Duch nám odhalí.

2. Zbor svätých volá „sláva, zvíťazil náš Pán“, aj spev anjelov zaniesol sa k nám.
Dnes aj z našich úst sa nová pieseň ozvala, nech zastaví sa čas a zneje chvála.

3. Bázeň ma preniká, veď mňa si zavolal, keď na okraji vinice som stál.

Teraz môžem klásť si tvoje plody do dlani a hľadať krásu v Božom dávaní.
Pozn.: slovenský preklad: K onnmita Emanuel

1308. Padol, chvíľu sa ta ckal, a potom Boh v boji, padol
H

e C e
R: Padol, chvíľu sa talckal, a potom Boh v boji, padol.

e C
1. Vybral som sa za tebou a predsa sm padol, pri chodníku našiel som kvet,
H

e
zanechal som teba i kríž, bol mi prekážkou.

2. Pre tento Tvoj druhý pád pomáhaj mi Pane, plniť dobre predsavzatia,
aby som Ťa neprinútil padnúť tretí raz.
3. Vojaci Ho bijú, leží bez pohybu, je už skoro na konci síl.

Vstaň, Pane, veď Ty musíš dôjsť, až na vrch Golgoty.
Pozn.: Medzi slo/ranu' sa Izespieva celý refrén, iba slovo Padol,

w Ježiš napomína (Plač, vzIyk a náreky)
C a F G C a F G C

1. Plač, vzlyk a náreky, len to im ostalo, keď pomôcť nemôžu, vidiac dnešné divadlo.
F G C

d G a d G
R: Neplačte nado mnou a nad mojimi ranami, plačte radšej nad sebou a nad svojimi vínami.

C a G _ C _ _ a F _ G C
2. Ja denne bedákam nad chybami blížnych, môj súd je prísnejší, neobstojí pred ním nik.

1310. Matka naša, Ty bolesťou trpíš a Tvoj syn Ti leží v náručí
a G a E'
R: Matka naša, Ty bolesťou trpíš a Tvoj syn Ti leží v náručí.
a a d E'
Matka naša, Ty Sedembolestná, Ty trpíš so Synom aj za nás.

C

   

E7

F a F
1. Zhliadni na nás, pomôž sa zbaviť pút, tých hriechov ťažkých, čo ničia k spáse púť.

F C a F E'
Pomôž by sme, by sme už mohli ísť, tým životom, jak učil nás Tvoj Syn.
c d G _ c _ E'

2. Pomôž matka, keď sme už zúfalí a nevieme v čo by sme dúfali,

a d E a
pros vždy za nás, pros u svojho Syna, On je naša jediná záchrana.

