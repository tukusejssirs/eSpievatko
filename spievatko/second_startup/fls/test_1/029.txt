IIS Jež

   

iš, aký si zázračný, si taký čistý

De_ A _ D h e A o o'
J ežiš, aký s1 zázračný, si taký čistý, dobrotivý.
D e A D h eA A' D GD

Svietiš ako ranná hviezda, Ježiš, aký si zázračný.
m Ako Iaň túži po bystrej vode
D e A D h A

D
1. Ako laň túži po bystrej vode, duša mi ťúži po Tebe, Bože.
Slzy sú chlieb môj v noci i vo dne. "Kdeže je Boh tvoj?" pýtaš sa denne.

o e _ A o_ h e A o
R: |:Po Bohu ŽÍZľllľľl, po Bohu žlvom. Po Bohu žízni duša moja.:|

2. Prečo si smutná, prečo sa chveješ. Len dúfaj v Pána, duša moja.
Veď všetky bolesti, smútok a tieseň, Pán všetko premení v radostnú pieseň.

m Kráľov Kráľ, nádherný, Boží syn

 

G (D) m" C D

Kráľov Kráľ, (2x) nádherný, (2x) Boží Syn, vzkriesený.

G (D) (F'“'") C a D
Pánov Pán, (2x), Boh silný, (2x) Kráľ a Boh, večný prameň lásky.

G o o D o c a o Co o a D
R: Chválim Ťa, môj Pane, chválim Ťa, len Ty ma podopneš, keď nie je sily ísť.

G D e C G

Chválim Ťa, môj Pane, chválim Ťa, Tvoja láska k nám je úžasná, úžasná.

E Prosíme Ťa, Bože, vyslyš prosby naše
e' e' C D G C

a H'
Prosíme Ťa Bože, vyslyš prosby naše, nádejou, posilní nás.
7 a H' e

e e G
Príď kráľovstvo Tvoje, buď vždy vôľa Tvoja, príď Tvoj pokoj do nás.
Nech zaznie chvála i česť a zvelebenie
D A

Nech zaznie chvála i česť a zvelebenie, chvála i česť Ježišovi,

D G g D  D _

chvála, nech zaznie chvála, chvála 1 česť Pánovi.

m Uprostred noci som držal hlavu
C F

d e
1. Uprostred noci som držal hlavu v dlaniach úplne obklopený šedivou hmlou.
E

a
A srdce s úzkosťou volalo: Príď, Pane, a zostaň stále so rrmou.

F G E
R: Drž ma Éa ruku a veď až do cieľa, každú hodinu a každý deň.
F

a G E a
Drž ma za ruku a podopieraj, nech nie som sám, s Tebou žiť chcem, ja stále chcem.

2. Ako kráľ by som mohol žiť v paláci, mať všetko, čo by som si prial.

No v tomto starom svete, môj Pane, nie je nič horšie, než byť sám, preto volám:

