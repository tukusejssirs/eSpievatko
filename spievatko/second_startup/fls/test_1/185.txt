 

Kamkoľvek pôjdem, Ty si vždy so mnou

A D A D A

. Kamkoľvek pôjdem, Ty si vždy so mnou, vedieš ma žitím, si mi ochranou.
E A E A
Vo dne, V noci, V každý čas, môžem volať zas a zas:
D E A A' D E A

Tvoja ruka, Pane, nado mnou. |:Tvoja ruka, Pane, nado mnou.:|(4x)

. Kamkoľvek pôjdeš aj Ty, priateľ môj, Pán Ježiš chce byť tvojou ochranou.

Vo dne v noci, v každý čas, môžeš volať zas a zas:
Tvoja ruka, Pane, nado mnou. |:Tvoja ruka, Pane, nado mnou.:|(4x)

 

Kam, kam, kam, kam sa tak ponáhľaš

text: Ondrej Studenec

A G” A A' D D“ D' D A

x Kam, kam, kam, kam sa tak ponáhľaš, ó, kamže, kam, kam, kam, prečo už nemáš čas,

G9 A G” A E' ft' A5* A A“ F” E” A E'
postoj, kam, kam, kam, no svoje dôvody nazývaj pravými menami.
A G9 A A'
. Čím viac sa náhliš, tým viac chvíľ cez prsty uniká Ti,
D D“ D' D“
ale ved' všetko má svoj čas, je čas siatia, je čas žatvy.
G' A G' A E' fr' A** A A' F' 5° A E'

D
Tvoje nohy stále blúdía, aj na Teba čaká Pán. čo ozajstný pokoj dáva nám. Kam, kam, kam, kam.

. Ó, Ty vždy tak ľahko nájdeš vodu, ktorá v smäd sa zmení,

ale je tu živá voda, po nej nebudeš viac smädný,
skús len chvíľu, otvor srdce, aj na Teba čaká Pán, čo ozajstný pokoj dáva nám. Kam, kam, kam, kam.

.' Kam, kam, kam, kam sa tak ponáhľaš, ó, kamže, kam, kam, kam, prečo už nemáš čas,

postoj, kam, kam, kam, aj na Teba čaká Pán, čo ozajstný pokoj dáva nám.

text: Taňa Horváthová

 

. |:Každý deň, každý deň, modlím sa za tri veci len:
A7

C h
. Každý deň, každý deň, modlím sa za tri veci len:
'I

Každý deň, každý deň, modlím sa za tri veci len

Fmaj? Ebmaj7Fmaj7 Ebrnaľldl C h

e A' d cm"
nech je viac radosti a detskej úprimnosti V každom z nás.:

d' C h

e
Nech je menej zlostí,
Fmal? Ebmaj? Fmaľl Ebmaj?

. Musíš chcieť, musíš chcieť, v každom dobro uvidieť.

Pridlho sme spali, na koho sme sa hrali, však aj ľudskí králi malí sú.

Final? ELmJJT Frnuj? Ebmnü d C h

. Viem, už viem, (viem, už viem), viem, už viem, vo všetkom tu je Boží tieň.

On dokáže povzniesť (každý deň) nad zúfalosť a bolesť (každý deň),
d c; cm"

kríž vládzeme uniesť, viem, už viem, (viem, už viem).

FmQJTELrMJT Fmal? ELmaH dl

e 0 o A o e s c  c

|:Nech je menej zlostí, (každý deň), nech je viac radosti, (každý deň):|(3x)

nech je viac radosti, (každý deň), nech je viac radosti, (každý deň),

d? Cmaj7 Fmaj? cmuj? Fmnj? Amaľf
a detskej úprimnosti v každom z nás, viem, už viem, už viem, už viem, verím.

