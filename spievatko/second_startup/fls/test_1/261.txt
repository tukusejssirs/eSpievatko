2. Nevidím, nevidím šťastie, iba zdanlivé, veľmi prchavé.
Nevidím, nevidím lásku, lásku trvalú, ktorá dáva. R:

3. Široká, široká cesta, cesta človeka, ľudských múdrosti,
ponúka, k slobode zve ťa, ktorá zotročí, vedie do smrti. R:

1339. Z Božej Lásky všetci sme, pre svet stvorení
C B F C B F

Z Božej Lásky všetci sme, pre svet stvorení. Z Božej Lásky sme v jedno telo spojení.

FG
Z Božej Lásky je každý deň darom pre nás Z Božej Lásky chceme vždy žiť, dýchať i rásť.

1340. O šťastí sníval, túlal sa po námestí, túlal sa v snení

 

C F G C F G C
1. O šťastí sníval, túlal sa po námestí, túlal sa v snení, ľudí však nemal rád.
F G C F G C
Vždy smutný býval, zrátal snáď všetky cesty. Raz zamyslený pred chrámom zostal stáť.
F c F
R1 .° Že tu v chráme bývaš, počul som jedenkrát, že sa v chlebe skrývaš,
G C F C
že vraj máš ľudí rád, ľudí rád, ľudí rád.
2. On prišiel k bráne a prešiel katedrálou, tam v svätom tichu Pánovi hovoril:

Tu ma máš Pane, prichádzam s prosbou malou mám V sebe pýchu., som slabý, nemám síl.

R2: Tak mi povedz prosím, povedz mi, ak si Pán.
Vieš, že smútok nosím, kde pokoj hľadať mám, hľadať mám, hľadať mám.

3. Tvoj pokoj skrytý tam medzi ľuďmi nájdeš, vtedy, keď smädným svoj pohár vody dáš.

Keď službou sýty pred tvár Pánovu zájdeš, keď ľuďom biednym obetuješ svoj plášť. R1:

1341. Golgota (Tu pod krížom zostávam rád)

a d E a
Golgota, Golgota, Golgota!

a d a d e' _c
1. Tu pod krížom zostávam rád, vplyv sveta sem nepreníká,
d a E'
strach, bolesť i žiaľ sa neznámym stal, keď v slzách na kríž pozerám.
a d G G' c _ E'
R: Klince nedržali na kríži Ťa, viem, ani ľudská zloba, môj Ťa držal hriech.
7 'I

a
Klince :Cdľžalĺ na kríži Ťa, viem, iba z :Ieľkej lásky ku mne si tam šiel.

2. Si spomínam na slávny deň, keď pod krížom padnúť som smel,
keď Boží mi Syn sňal bremeno vín, v jas premenil strašlivý tieň. R:

3. Ja na Teba hľadím, môj Pán, jak plný si bolestí, rán.
Tá milosti moc! Už minula noc, dnes od hriechov slobodu mám. R:

