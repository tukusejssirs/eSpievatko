Nesieme, Pane, chlieb a víno
d _ _ C a _

Nesieme, Pane, chlieb a víno. Nesieme celý život náš.

d d a d

     

hudba a text: Jiří Černý

C a
Nesieme, Pane, chlieb a víno a hľadáme |:Tvoju tvár,:| Tvoju svätú tvár.
Glória, Patri et Filio

 

D_A_h_f#GD_E__AD A hfiGDAD
Glória, glória, glória, Patri et Filio. Glória, glória, glória, Spiritui Sancto.

Sme deti narodené z utrpenia i hudba: G. Kendrick m
C F G C C F

G G'
1. Sme deti narodené z utrpenia, krvou nás vykúpil Pán,
C F G C F G F GG7
chce, by sme boli pre národy svetlom, v lásku a moc zaodel nás.
a G F a G F
R: V Jeho mene chod“, len Ježiš je náš Pán,
d G C F C F G

a C C
už je čas, by Cirkev povstala, hlásala: "Ježiš Kristus vykúpil a spasil nás!"
2. Je veľa duší, čo trápia sa vo tme. Či chceš spať, ked' svetlo máš ?
Ved' Ježiš prikázal svedčiť o pravde. To je náš cieľ, náš veľký cieľ.

3. Počuj, ten vánokje znak Jeho Ducha, blíži sa posledný súd,
CaF G
základy zeme i neba sa trasú, príde pre nás, vráti sa zas. Ježiš náš Pán. (3x)

Kto stvoril žmurkajúce hviezdy . i m
G G G. G . . G . . G . .

1. Kto stvoril žmurkajúce hviezdy, žmurkajúce hviezdy, žmurkajúce hviezdy?
G o c_ G _ _ c D G
Kto stvoril žmurkajúce hviezdy? náš Otec Boh.

2. ...vlniace sa more. 3. ...líetajúcich vtáčkov. 4. ...teba, jeho i mňa.
Slnko vyšlo, je tu deň í
h A D A D

e h e
l. Slnko vyšlo, je tu deň, že Boh rád ma má, to viem. Chcem ho prežiť V milosti, nechje ako sen.
2. Vtáčik lieta v záhrade, nevie, čo je blúdiť v tme. Miesto hriechov, nerestí žije v radosti.

G A D G _ A _ D

R: Život krásny je v taký deň, keď Božiu vôľu plniť chcem,
h e D
dá mi silu vytrvať, ak chcem žiť len preň

3. Ná ná ná .... ..už chcem žiť len preň. Daj nech lásku šíriť viem, Pane, každý deň.

   

Ježiš žije. Všetci ľudia nech to vedia

A . .. . G.. .A . Gusi ŠŠAEA)
R: Ježiš žije! Ježiš žije! všetci ľudia nech to vedia: Jezis zije! (2x)

 (WW) _ _ _
1. Všetci z mŕtvych vstaneme, (3x) jak On. Aleluja, bratia!
D A

A E A
Vtedy, až sa skončí čas, Ježiš Kristus príde zas a iny všetci z mŕtvych vstaneme jak On.

2. A On s nami zostane, (3x) náš Pán. Aleluja, bratia!
Nech sa každý pridá k nám, nikto nemusí byť sám, Ježiš s nami zostane, jak sľúbil nám.

