Spievaj Haleluja Pánovi, spievaj Haleluja Pánovi m
a G C

 

a E E'

Spievaj Haleluja Pánovi, spievaj Haleluja Pánovi,

a _ _ G_ _ C a G a E

spievaj Haleluja, spievaj Haleluja, spievaj Haleluja Pánovi.

Spoj nás v jedno, Pane, spoj nás jedným putom hudba: Bob Gi/Imann m
D h e A

Spoj nás v jedno, Pane, spoj nás jedným putom, čo nemôže byť zlámané.

e A' D

Spoj nás V jedno, Pane, spoj nás jedným putom, spoj nás tou bratskou láskou.
D A' fl* hD A' G D D A' fl* hD A' D
Je len jeden Boh, je lenjedno Telo, je lenjeden Kráľ, preto spievajme:

Na svojich cestách ju stretám i ked' ju nevidieťudba: Fero Horváth. text: M. Pastorová

   

a
1. Na svojich cestách ju stretám i keď ju nevidieť,

d e A
stretám ju práve tam, kde obzerám sa späť, kde váham, či ísť ďalej, hoc temer nemám síl.
D' F a E F D' E F G FG FG FG C

Mária, Mária, Mária mi vraví, aby som sa pokúsil ísť ďalej.
, aC aC _FC FC a_FaF a_FaF
R: Ano, Pane, staň sa, Pane, Tvoja vôľa nech sa stane, už nie ja, lež Ty, už nie ja, lež Ty.
C a _ F D' C E a_  a F aa
Za Tebou stále isť, nezastať, nezblúdiť, zvíťaziť. Už nie ja, lež Ty!
2. Na svojich cestách ju stretám i keď ju nevidieť,
stretám ju najmä tam, kde hľadám odpoveď: načo meniť svoj život, ak som ho dobre žil?
Mária, Mária, Mária mi vraví, aby som sa pokúsil ísť ďalej. R:
Stavitelia miest (Načo sa namáhate) hudba a text: Richard Čanaky m

a F C G
l. Načo sa namáhate, stavitelia miest, načo tu strácate svoj drahý čas.

      

a F C
Veď je Vám jedno, že tým budujete hriech, bez Boha staviate svoj vlastný krach.

a F C G a F C _ G _
R: ltTy, Bože, jediný máš právo nad iným, znič dielo satana a lož nech je zv1azaná.:|

2. Darmo chceš strážiť mesto, čo si vystaval, všetko je prázdnota, tak načo bdieť.
Darmo sa namáhaš a myslíš, že si kráľ, nemôžeš život žiť na vlastnú päsť.
Album: Richard Čanaky: Vďaka Ježiš

 

Stále len chcem si spievať... že Pán tu medzi nami je E
Intro: GCGC
c . . , G . o ,
R: Stale len chcem Sl spievať, stale len chcem Sl spievat.
C a' A'
Stále len chcem si spievať, že Pán tu medzi nami je.
G G'/HG'IH c a' GG G'Iu_ _ a' _ D _ G
1 . My vzdialili sme sa mu a nechali ho stáť, on predsa vždy sa vracia, je medzi nami rad.

2. On nestavia si domy, zámok ho nestráži, keď lásku k blížnym máme, sme v jeho prístreší.
A2

3. Radostne zahrajte mu, spievajte, kto ste tu, dlaňami zatlieskajte, volajte Pán je tu.
Album: Ježi: Stále len chcem si spievat'

