m Izrael (Mohol si byt' slávna hviezda) i hudbaa text: Richard Čanaky

a _ F _ G“ G“ _ e' _
1. Mohol Sl byť slávna hviezda, teraz VlSĺŠ na krížikoch,
minul si sa trošku cieľa, hneď si mohol na piedestál ísť.

2. Načo si sa sklonil z neba, keď ťa vlastný neprijali,
bola chybná atmosféra, mohol si do Ameriky prísť.

3. Čakali ťa V Holywoode, hlavnú rolu mal si vtedy hrať,
ako mocný vládca zeme mal si všetky vojny vyhrávať.

4. Mohol si byť slávna hviezda, ovenčený zlatom, striebrom,
Ty si šiel do Izraela, priamo na kríž, ako dáky blázon.

R: Izrael, Izrael, Izrael, Izrael.

4. Ako rytier v ruke s mečom, na koni si mohol radšej prísť,
Ty však ako slabé dieťa chcel si nad zlom navždy zvíťaziť.

5. Dodnes neviem, prečo láka Tvoja láska moje túžby,
viem, že dnes Ti potrím celý, nech si myslí, kto čo si chce sám. R:

6. Neviem veru, či si blázon, život si však môjmu srdcu dal,
ako dieťa kľudne spinká, tak sa cítim, keď prichádzaš k nám.

7. Takých ako ja je mnoho, my sme blázni pre tento svet,
nemusíme veriť v bohov, ktorý mlčia, v takých láska niet. R:

8. Mohol si byť slávna hviezda, lenže Ty si krížu prednosť dal,
dobrá bola atmosféra, dnes Ti patrí trón aj piedestál.

9. Požehnaj tých zopár duší, čo počujú túto pieseň,
možno niekto z nich už tuší, že si na kríži pre nich zomrel rád. R:
Album: Konzpro/nis: Právo na lásku
Jasaj celá zem (Pozri a pochop) text: Ondrej Studenec
c _a' F _GC G' c a7_ F GC G' c
1. Pozri a pochop, čo stvoril náš dobrý Pán, kvety, zvieratá, stromy, náš dobrý Pán.
Kto duši život dal darom, náš dobrý Pán, a chráni nohy pred pádom, náš dobrý Pán.

G7 C eT aT cdlm C9 G7
R: A preto |:jasaj celá zem, Bohu spievaj len
C F C' C G' C G7

(radostne) ohlasuj slávu jeho mena a diela úžasné. (tak poď a):|
2. Poďte a počujte všetci, náš dobrý Pán, aké veľké mi urobil veci, náš dobrý Pán.
Keď som slabý a na dne, náš dobrý Pán, On vyslyšal modlitby moje, náš dObľý Pán. R:

3. Ak chceš, aby V skúškach si obstál, náš dobrý Pán, pomocnú ruku Ti podá, náš dobrý Pán. R:
Album: Céčko: Nebojíme sa

