2. Deň za dňom kráča, noc slzy zmáča, spoznávam, že som biedny.

Vstávairi a padám, tú cestu hľadám, ktorá ma vyslobodí.

Mám túžbu v srdci vyjsť z tmavej noci do svetla, ktoré svieti.
Hlas tichý, láskavý, volá ma, vraví: "Život dám ti".

Volá že: "Mám ťa rád, to všetko robím pre teba.

Mám ťa rád, veď život dal som za teba - som Ježiš."

1008. Resta con noi, O Signore, che gia scende la sera 

h E°lrt _ h E°IH h_ A D F9 h E°/ł_-| _ h E°IH h_ F3 h F“ '
Resta con noi, O Signore, clie gia scende la sera. Resta con noi, O Signore, che gia scende la sera.

Preklad: ZOJFIÚŇ s nami, ó, Pane, lebo sa zvečerieva.

 

 

Ríša Rím veľká, cisár rozkaz svoj vydal, sčítať sa má i text: I. Šimko
G D G D G C_G hG _ D
. Ríša Rím veľká, cisár rozkaz svoj vydal, sčítať sa má.
_ H' e H' e a _ e _ H' e
A každý jeden národ počet vydá a preto každý kraj rodu svojho hľadá.

. Mesto Betlehem malé, pre toľko ľudí podmienky zlé,
tehotná Mária nemá kam ísť. pretože je tiež deň, ked' jej čas má už prísť.
. Cudzích ľudí a zhon stretá, ked' sa s láskou Boh ľuďom dá,
ktovie či cenzor si aj Jeho hlas vzal, taký biedny je ten, čo je na veky Kráľ.

 

Rozhodni sa (Nik z nás nemôže slúžiť dvom pánom)
D _ e/o Alp _ D e/o _ A/o
. Nik z nás nemôže služiť dvom pánom, nemôžme slúžiť naraz dvom kráľom.
Flo E/o Eblo D e/o A/D D
Bud' jedného máš rád, druhého podvádzaš, zastav sa nad týmto, kam vlastne kráčaš.

. Postoj a počúvaj, čo Boh dnes vraví, preskúmaj srdce dnes, komu asi patrí.
Rozhodni sa sám, komu slúžiť chceš, Boh teraz volá ťa, jemu patriť smieš.

. Učiň srdce stálym byť, slúžiť vždy Bohu. Zanechaj tento svet, hriech, mámosť, zlobu.

Rozhodni sa sám, komu slúžiť chceš, Boh teraz volá ťa, jemu patriť smieš.
Album: Radostná srdce: Uzka brána

E Rozžíhá Maria svíce v nás, temnotou s Marií projdem snáz

 

c G _ d a c G _ d _ a
R: Rozžíhá Maria svíce v nás, temnotou s Marií projdem snáz.
F d C G C G F C

Rozžíhá Maria svíce v nás, temnotou s Marií projdem snáz.
a F d C a F e G' G
I. J í patří dík, že mŕtvým lánům dal život Boží Syn, že světlo vešlo do míst, kde dlouho ležel stín.
2. J í patří dík, že Země k nebi se znovu přiklání, že padla vláda noci a přišlo svítaní.
3. Jí patří dík, že Věčné Světlo nám lioří V kostelích a nebýt Matky Boží, kdo znal by Knihu Knih?
Pozn.: Hradec Králové '97

